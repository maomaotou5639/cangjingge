/*
Navicat MySQL Data Transfer

Source Server         : win10MariaDB
Source Server Version : 50505
Source Host           : localhost:3307
Source Database       : book

Target Server Type    : MYSQL
Target Server Version : 50505
File Encoding         : 65001

Date: 2021-08-24 19:36:36
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for book_cat
-- ----------------------------
DROP TABLE IF EXISTS `book_cat`;
CREATE TABLE `book_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book_cat
-- ----------------------------
INSERT INTO `book_cat` VALUES ('1', '0', '综合馆', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('2', '0', '童书馆', '1', '1', '2021-03-29 18:09:38', '2021-08-10 23:16:55');
INSERT INTO `book_cat` VALUES ('3', '0', '生活馆', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('4', '0', '教育馆', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('5', '0', '艺术馆', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('6', '0', '科技馆', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('7', '1', '小说', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('8', '1', '文学', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('9', '1', '传记', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('10', '2', '儿童文学', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('11', '2', '幼儿启蒙', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('12', '2', '科普百科', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('13', '2', '卡通动漫', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('14', '3', '励志书籍', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('15', '3', '养生大法', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('16', '3', '家常菜谱', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('17', '4', '外语学习', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `book_cat` VALUES ('18', '4', '大中专教材', '1', '2', '2021-07-11 23:27:03', '2021-07-11 23:27:03');
INSERT INTO `book_cat` VALUES ('19', '4', '字典词典', '1', '2', '2021-07-11 23:27:03', '2021-07-11 23:27:03');
INSERT INTO `book_cat` VALUES ('20', '4', '中小学教材', '1', '2', '2021-07-11 23:27:03', '2021-07-11 23:27:03');
INSERT INTO `book_cat` VALUES ('21', '5', '绘画', '1', '2', '2021-07-11 23:27:03', '2021-07-11 23:27:03');
INSERT INTO `book_cat` VALUES ('22', '5', '摄影', '1', '2', '2021-07-11 23:27:03', '2021-07-11 23:27:03');
INSERT INTO `book_cat` VALUES ('23', '5', '设计', '1', '2', '2021-07-11 23:27:03', '2021-07-11 23:27:03');
INSERT INTO `book_cat` VALUES ('24', '5', '音乐', '1', '2', '2021-07-11 23:27:03', '2021-07-11 23:27:03');
INSERT INTO `book_cat` VALUES ('25', '6', '建筑', '1', '2', '2021-07-11 23:27:03', '2021-07-11 23:27:03');
INSERT INTO `book_cat` VALUES ('26', '6', '医学', '1', '2', '2021-07-11 23:27:03', '2021-07-11 23:27:03');
INSERT INTO `book_cat` VALUES ('27', '6', '电子科技', '1', '2', '2021-07-11 23:27:03', '2021-07-11 23:27:03');
INSERT INTO `book_cat` VALUES ('28', '6', '编程语言', '1', '2', '2021-07-11 23:27:03', '2021-07-11 23:27:03');

-- ----------------------------
-- Table structure for book_info
-- ----------------------------
DROP TABLE IF EXISTS `book_info`;
CREATE TABLE `book_info` (
  `book_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `author` varchar(15) NOT NULL,
  `isbn` varchar(15) NOT NULL,
  `introduction` text DEFAULT NULL,
  `language` varchar(4) NOT NULL,
  `price` varchar(10) NOT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `number` int(6) DEFAULT NULL,
  `images` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`book_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of book_info
-- ----------------------------
INSERT INTO `book_info` VALUES ('1', '大雪中的山庄', '东野圭吾 ', '9787530216835', '东野圭吾长篇小说杰作，中文简体首次出版。 一出没有剧本的舞台剧，为什么能让七个演员赌上全部人生.东野圭吾就是有这样过人的本领，能从充满悬念的案子写出荡气回肠的情感，在极其周密曲折的同时写出人性的黑暗与美丽。 一家与外界隔绝的民宿里，七个演员被要求住满四天，接受导演的考验，但不断有人失踪。难道这并非正常排练，而是有人布下陷阱要杀他们。 那时候我开始喜欢上戏剧和音乐，《大雪中的山庄》一书的灵感就来源于此。我相信这次的诡计肯定会让人大吃一惊。——东野圭吾', '中文', '35.00', '9', '0', null);
INSERT INTO `book_info` VALUES ('2', '三生三世 十里桃花', '唐七公子 ', '9787544138000', '三生三世，她和他，是否注定背负一段纠缠的姻缘？\r\n三生三世，她和他，是否终能互许一个生生世世的承诺？', '中文', '26.80', '7', '0', null);
INSERT INTO `book_info` VALUES ('3', '何以笙箫默', '顾漫 ', '9787505414709', '一段年少时的爱恋，牵出一生的纠缠。大学时代的赵默笙阳光灿烂，对法学系大才子何以琛一见倾心，开朗直率的她拔足倒追，终于使才气出众的他为她停留驻足。然而，不善表达的他终于使她在一次伤心之下远走他乡……', '中文', '15.00', '7', '1', null);
INSERT INTO `book_info` VALUES ('4', '11处特工皇妃', '潇湘冬儿', '9787539943893', '《11处特工皇妃(套装上中下册)》内容简介：她是国安局军情十一处惊才绝艳的王牌军师——收集情报、策划部署、进不友好国家布置暗杀任务……她运筹帷幄之中，决胜于千里之外，堪称军情局大厦的定海神针。', '中文', '74.80', '7', '1', null);
INSERT INTO `book_info` VALUES ('5', '人类简史', '[以色列] 尤瓦尔·赫拉利 ', '9787508647357', '十万年前，地球上至少有六种不同的人\r\n但今日，世界舞台为什么只剩下了我们自己？\r\n从只能啃食虎狼吃剩的残骨的猿人，到跃居食物链顶端的智人，\r\n从雪维洞穴壁上的原始人手印，到阿姆斯壮踩上月球的脚印，\r\n从认知革命、农业革命，到科学革命、生物科技革命，\r\n我们如何登上世界舞台成为万物之灵的？\r\n从公元前1776年的《汉摩拉比法典》，到1776年的美国独立宣言，\r\n从帝国主义、资本主义，到自由主义、消费主义，\r\n从兽欲，到物欲，从兽性、人性，到神性，\r\n我们了解自己吗？我们过得更快乐吗？\r\n我们究竟希望自己得到什么、变成什么？', '英文', '68.00', '11', '1', null);
INSERT INTO `book_info` VALUES ('6', '明朝那些事儿（1-9）', '当年明月 ', '9787801656087', '《明朝那些事儿》讲述从1344年到1644年，明朝三百年间的历史。作品以史料为基础，以年代和具体人物为主线，运用小说的笔法，对明朝十七帝和其他王公权贵和小人物的命运进行全景展示，尤其对官场政治、战争、帝王心术着墨最多。作品也是一部明朝政治经济制度、人伦道德的演义。', '中文', '358.20', '11', '2', null);
INSERT INTO `book_info` VALUES ('7', '经济学原理（上下）', '[美] 曼昆 ', '9787111126768', '此《经济学原理》的第3版把较多篇幅用于应用与政策，较少篇幅用于正规的经济理论。书中主要从供给与需求、企业行为与消费者选择理论、长期经济增长与短期经济波动以及宏观经济政策等角度深入浅出地剖析了经济学家们的世界观。', '英文', '88.00', '6', '1', null);
INSERT INTO `book_info` VALUES ('8', '方向', '马克-安托万·马修 ', '9787020125265', '《方向》即便在马修的作品中也算得最独特的：不着一字，尽得风流。原作本无一字，标题只是一个→，出版时才加了个书名Sens——既可以指“方向”，也可以指“意义”。 《方向》没有“字”，但有自己的语言——请读者在尽情释放想象力和独立思考之余，破解作者的密码，听听作者对荒诞的看法。', '中文', '99.80', '9', '1', null);
INSERT INTO `book_info` VALUES ('9', '画的秘密', '马克-安托万·马修 ', '9787550265608', '一本关于友情的疗伤图像小说，直击人内心最为隐秘的情感。 一部追寻艺术的纸上悬疑电影，揭示命运宇宙中奇诡的真相。 ★ 《画的秘密》荣获欧洲第二大漫画节“瑞士谢尔漫画节最佳作品奖”。 作者曾两度夺得安古兰国际漫画节重要奖项。 ★ 《画的秘密》是一部罕见的、结合了拼贴、镜像、3D等叙事手法的实验型漫画作品。作者巧妙地调度光线、纬度、时间、记忆，在一个悬念重重又温情治愈的故事中，注入了一个有关命运的哲学议题。', '中文', '60.00', '9', '0', null);
INSERT INTO `book_info` VALUES ('10', '造彩虹的人', '东野圭吾 ', '9787530216859', '其实每个人身上都会发光，但只有纯粹渴求光芒的人才能看到。 从那一刻起，人生会发生奇妙的转折。功一高中退学后无所事事，加入暴走族消极度日；政史备战高考却无法集中精神，几近崩溃；辉美因家庭不和对生活失去勇气，决定自杀。面对糟糕的人生，他们无所适从，直到一天夜里，一道如同彩虹的光闯进视野。 凝视着那道光，原本几乎要耗尽的气力，源源不断地涌了出来。一切又开始充满希望。打起精神来，不能输。到这儿来呀，快来呀——那道光低声呼唤着。 他们追逐着呼唤，到达一座楼顶，看到一个人正用七彩绚烂的光束演奏出奇妙的旋律。 他们没想到，这一晚看到的彩虹，会让自己的人生彻底转...', '中文', '39.50', '9', '1', null);
INSERT INTO `book_info` VALUES ('11', '控方证人', '阿加莎·克里斯蒂 ', '9787513325745', '经典同名话剧六十年常演不衰； 比利•怀尔德执导同名电影，获奥斯卡金像奖六项提名！ 阿加莎对神秘事物的向往大约来自于一种女性祖先的遗传，在足不出户的生活里，生出对世界又好奇又恐惧的幻想。 ——王安忆 伦纳德•沃尔被控谋杀富婆艾米丽以图染指其巨额遗产，他却一再表明自己的无辜。伦纳德的妻子是唯一能够证明他无罪的证人，却以控方证人的身份出庭指证其确实犯有谋杀罪。伦纳德几乎陷入绝境，直到一个神秘女人的出现…… 墙上的犬形图案；召唤死亡的收音机；蓝色瓷罐的秘密；一只疯狂的灰猫……十一篇神秘的怪谈，最可怕的不是“幽灵”，而是你心中的魔鬼。', '中文', '35.00', '9', '1', null);
INSERT INTO `book_info` VALUES ('12', '少有人走的路', 'M·斯科特·派克 ', '9787807023777', '这本书处处透露出沟通与理解的意味，它跨越时代限制，帮助我们探索爱的本质，引导我们过上崭新，宁静而丰富的生活；它帮助我们学习爱，也学习独立；它教诲我们成为更称职的、更有理解心的父母。归根到底，它告诉我们怎样找到真正的自我。 正如开篇所言：人生苦难重重。M·斯科特·派克让我们更加清楚：人生是一场艰辛之旅，心智成熟的旅程相当漫长。但是，他没有让我们感到恐惧，相反，他带领我们去经历一系列艰难乃至痛苦的转变，最终达到自我认知的更高境界。', '中文', '26.00', '9', '1', null);
INSERT INTO `book_info` VALUES ('13', '追寻生命的意义', '[奥] 维克多·弗兰克 ', '9787501162734', '《追寻生命的意义》是一个人面对巨大的苦难时，用来拯救自己的内在世界，同时也是一个关于每个人存在的价值和能者多劳们生存的社会所应担负职责的思考。本书对于每一个想要了解我们这个时代的人来说，都是一部必不可少的读物。这是一部令人鼓舞的杰作……他是一个不可思议的人，任何人都可以从他无比痛苦的经历中，获得拯救自己的经验……高度推荐。', '中文', '12.00', '9', '0', null);
INSERT INTO `book_info` VALUES ('14', '秘密花园', '乔汉娜·贝斯福 ', '9787550252585', '欢迎来到秘密花园！ 在这个笔墨编织出的美丽世界中漫步吧 涂上你喜爱的颜色，为花园带来生机和活力 发现隐藏其中的各类小生物，与它们共舞 激活创造力，描绘那些未完成的仙踪秘境 各个年龄段的艺术家和“园丁”都可以来尝试喔！', '中文', '42.00', '9', '1', null);

-- ----------------------------
-- Table structure for borrow
-- ----------------------------
DROP TABLE IF EXISTS `borrow`;
CREATE TABLE `borrow` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(30) DEFAULT '',
  `bookname` varchar(60) DEFAULT '',
  `phone` varchar(11) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `status` tinyint(1) DEFAULT 1,
  `authorname` varchar(20) DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of borrow
-- ----------------------------
INSERT INTO `borrow` VALUES ('1', 'shang', '起源', '13666172295', '2021-08-10 10:07:57', '1', '1', '2021-08-29 20:51:16');
INSERT INTO `borrow` VALUES ('6', '委任为', '让外人', '13666172296', '2021-08-11 00:00:00', '1', '1', '2021-08-29 20:51:20');
INSERT INTO `borrow` VALUES ('7', 'shang', '终结', '13666172290', '2021-08-10 16:42:00', '1', '1', '2021-08-29 20:51:25');
INSERT INTO `borrow` VALUES ('8', '殇', '万古神帝', '13666172295', '2021-08-11 00:25:00', '1', '1', '2021-08-29 20:51:28');
INSERT INTO `borrow` VALUES ('9', '殇', '100个放弃理由', '13666172295', '2021-08-10 12:45:00', '1', '1', '2021-08-29 20:51:32');
INSERT INTO `borrow` VALUES ('10', '殇', '54545', '13666123456', '2021-08-10 10:02:00', '1', '1', '2021-08-29 20:51:35');
INSERT INTO `borrow` VALUES ('11', '企鹅去', '驱蚊器', '13456789521', '2021-08-11 00:00:00', '1', '1', '2021-08-29 20:51:39');
INSERT INTO `borrow` VALUES ('12', '恶趣味去', '驱蚊器', '13666123456', '2021-08-11 00:05:00', '1', '1', '2021-08-29 20:51:43');
INSERT INTO `borrow` VALUES ('13', '阿松大', '感到翻跟斗', '12345678901', '2021-08-11 00:48:00', '1', '1', '2021-08-29 20:51:46');
INSERT INTO `borrow` VALUES ('14', '恶趣味', '驱蚊器', '13666172265', '2021-08-11 00:48:00', '1', '1', null);
INSERT INTO `borrow` VALUES ('15', '切尔茜', '切尔茜', '13666123458', '2021-08-11 00:50:00', '1', '1', null);
INSERT INTO `borrow` VALUES ('16', 'qeq', 'qeq', '13666123546', '2021-08-11 00:52:00', '1', '1', null);
INSERT INTO `borrow` VALUES ('17', '杨杰', '道德经', null, '2021-08-12 15:42:01', '1', null, null);
INSERT INTO `borrow` VALUES ('18', '杨杰', '道德经', null, '2021-08-12 15:42:01', '1', null, null);

-- ----------------------------
-- Table structure for consumer
-- ----------------------------
DROP TABLE IF EXISTS `consumer`;
CREATE TABLE `consumer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(20) DEFAULT NULL,
  `password` varchar(100) DEFAULT '',
  `phone` varchar(11) DEFAULT NULL,
  `username` varchar(30) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of consumer
-- ----------------------------
INSERT INTO `consumer` VALUES ('8', 'a@qq.com', 'e10adc3949ba59abbe56e057f20f883e', '12345678901', 'admin', '1');
INSERT INTO `consumer` VALUES ('9', 'aa@qq.com', '202cb962ac59075b964b07152d234b70', '12345678901', 'admin2', '1');
INSERT INTO `consumer` VALUES ('10', 'A@qq.com', 'e10adc3949ba59abbe56e057f20f883e', '12345678901', 'admin3', '1');

-- ----------------------------
-- Table structure for item_cat
-- ----------------------------
DROP TABLE IF EXISTS `item_cat`;
CREATE TABLE `item_cat` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_id` int(11) DEFAULT NULL,
  `name` varchar(150) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1195 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of item_cat
-- ----------------------------
INSERT INTO `item_cat` VALUES ('1', '0', '图书、音像、电子书刊', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('2', '1', '电子书刊', '1', '2', '2021-03-29 18:09:38', '2021-08-10 23:16:55');
INSERT INTO `item_cat` VALUES ('7', '1', '音像', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('11', '1', '英文原版', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('18', '1', '文艺', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('24', '1', '少儿', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('30', '1', '人文社科', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('40', '1', '经管励志', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('45', '1', '生活', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('51', '1', '科技', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('59', '1', '教育', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('65', '1', '港台图书', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('70', '1', '其它', '1', '2', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('74', '0', '政治、法律', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('161', '0', '文化、科学、教育、体育', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('249', '0', '马克思主义、列宁主义、毛泽东思想、邓小平理论', '1', '1', '2021-03-29 18:09:38', '2021-08-10 23:37:04');
INSERT INTO `item_cat` VALUES ('290', '0', '哲学、宗教', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('296', '0', '母婴', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('378', '0', '经济', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('438', '0', '军事', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('495', '0', '社会科学总论', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('558', '0', '文学', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('580', '0', '语言、文字', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('633', '0', '家居家装', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('699', '0', '历史、地理', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('749', '0', '自然科学总论', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('865', '0', '数理科学和化学', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('903', '0', ' 医药、卫生', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('963', '0', '天文学、地球科学', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('1031', '0', '艺术', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('1147', '0', '生物科学', '1', '1', '2021-03-29 18:09:38', '2021-03-29 18:10:02');
INSERT INTO `item_cat` VALUES ('1183', '0', '农业科学', '1', '1', '2021-07-11 23:27:03', '2021-07-11 23:27:03');
INSERT INTO `item_cat` VALUES ('1184', '1183', '22', '1', '2', '2021-07-11 23:27:12', '2021-07-11 23:27:12');
INSERT INTO `item_cat` VALUES ('1185', '1183', '22', '1', '2', '2021-07-11 23:27:19', '2021-07-11 23:27:19');
INSERT INTO `item_cat` VALUES ('1186', '1183', '22222', '1', '2', '2021-07-11 23:27:26', '2021-07-11 23:27:26');
INSERT INTO `item_cat` VALUES ('1194', '1186', '333', '1', '3', '2021-08-17 00:39:51', null);

-- ----------------------------
-- Table structure for item_desc
-- ----------------------------
DROP TABLE IF EXISTS `item_desc`;
CREATE TABLE `item_desc` (
  `id` int(11) NOT NULL COMMENT '商品ID',
  `item_desc` text DEFAULT NULL COMMENT '商品描述',
  `created` datetime DEFAULT NULL COMMENT '创建时间',
  `updated` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`),
  KEY `item_id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='商品描述表';

-- ----------------------------
-- Records of item_desc
-- ----------------------------
INSERT INTO `item_desc` VALUES ('6', '<ul><li>品牌：&nbsp;<a href=\"https://list.jd.com/list.html?cat=9987,653,655&amp;ev=exbrand_8557\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(94, 105, 173);\">华为（HUAWEI）</a></li><li>商品名称：华为Mate 40 Pro</li><li>商品编号：100009424247</li><li>商品毛重：0.575kg</li><li>商品产地：中国大陆</li><li>CPU型号：其他</li><li>运行内存：8GB</li><li>机身存储：128GB</li><li>存储卡：NM存储卡</li><li>摄像头数量：后置三摄</li><li>后摄主摄像素：5000万像素</li><li>前摄主摄像素：1300万像素</li><li>主屏幕尺寸（英寸）：其他英寸</li><li>分辨率：其它分辨率</li><li>屏幕比例：其它屏幕比例</li><li>屏幕前摄组合：其他</li><li>充电器：10V/4A；4.5V/5A；11V/6A；5V/2A；10V/2.25A；5V/4.5A；9V/2A</li><li>热点：无线充电，快速充电，防水防尘，NFC，屏幕指纹，高倍率变焦，曲面屏，5G，3D(ToF或结构光)，屏幕高刷新率</li><li>操作系统：其他OS</li></ul><p class=\"ql-align-right\"><a href=\"https://item.jd.com/100009424247.html#product-detail\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(0, 90, 160);\">更多参数&gt;&gt;</a></p><p class=\"ql-align-center\"><a href=\"https://prodev.jd.com/mall/active/4MmNbTkve7YDv3cqfN65jyU54rSB/index.html\" rel=\"noopener noreferrer\" target=\"_blank\" style=\"color: rgb(102, 102, 102);\"><img src=\"https://img30.360buyimg.com/jgsq-productsoa/jfs/t1/175410/16/1288/138398/60643912Eb556f8dd/b5dddd77bfb1189d.jpg\"></a></p><p><br></p><p><br></p><p><br></p><p class=\"ql-align-center\"><img src=\"https://img30.360buyimg.com/sku/jfs/t1/171353/28/18918/597397/60792c26Ea7af3162/a3d01d86a26914c7.jpg\" height=\"1665\" width=\"750\"><img src=\"https://img30.360buyimg.com/sku/jfs/t1/170519/21/18918/252212/60792c26E3ffeec48/e72803fd0e0ee024.jpg\" height=\"1040\" width=\"750\"><img src=\"https://img30.360buyimg.com/sku/jfs/t1/127525/12/15934/192855/5f92ad88Ef1d8a1d0/308eeb396c1b4f0c.jpg\" height=\"980\" width=\"750\"><img src=\"https://img30.360buyimg.com/sku/jfs/t1/127737/29/16107/53864/5f92ad87E2f31013f/296cc6da2147e8c6.jpg\" height=\"980\" width=\"750\"><img src=\"https://img30.360buyimg.com/sku/jfs/t1/150644/33/4055/87380/5f92ad88Ec6dc8304/bcc4722ace0f3a26.jpg\" height=\"980\" width=\"750\"><img src=\"https://img30.360buyimg.com/sku/jfs/t1/138990/29/11931/107588/5f92ad88E8f593e81/aa10f6fb142f1cc7.jpg\" height=\"980\" width=\"750\"><img src=\"https://img30.360buyimg.com/sku/jfs/t1/147316/22/11802/90171/5f92ad88E126be777/49a8533ab4b2a0f1.jpg\" height=\"980\" width=\"750\"></p><p><br></p>', '2021-04-16 17:10:53', '2021-04-16 17:10:53');
INSERT INTO `item_desc` VALUES ('7', '<iframe class=\"ql-video\" frameborder=\"0\" allowfullscreen=\"true\" src=\"https://vod.300hu.com/4c1f7a6atransbjngwcloud1oss/70da0da8353541978833035265/v.f30.mp4?source=1&amp;h265=v.f1022_h265.mp4\"></iframe><p><br></p>', '2021-04-19 11:02:01', '2021-04-19 11:02:01');
INSERT INTO `item_desc` VALUES ('8', '<ul><li>品牌：&nbsp;<a href=\"https://list.jd.com/list.html?cat=9987,653,655&amp;ev=exbrand_316578\" target=\"_blank\" style=\"color: rgb(94, 105, 173);\">克里特（kreta）</a></li><li>商品名称：克里特（kreta）12Pro</li><li>商品编号：10023875891222</li><li>店铺：&nbsp;<a href=\"https://kreta.jd.com/\" target=\"_blank\" style=\"color: rgb(94, 105, 173);\">克里特KRETA手机官方旗舰店</a></li><li>商品毛重：0.8kg</li><li>商品产地：中国大陆</li><li>货号：12Pro</li><li>操作系统：Android(安卓)</li></ul><p class=\"ql-align-right\"><a href=\"https://item.jd.com/10023875891222.html#product-detail\" target=\"_blank\" style=\"color: rgb(0, 90, 160);\">更多参数&gt;&gt;</a></p><p><img src=\"https://img11.360buyimg.com/devfe/jfs/t19249/266/555939662/10324/447efd03/5a965eb2Nf83fd68c.jpg\" alt=\"品质生活\"></p><ul><li class=\"ql-align-center\"><a href=\"https://item.jd.com/10023875891222.html#none\" target=\"_blank\" style=\"color: rgb(102, 102, 102); background-color: initial;\"><img src=\"https://static.360buyimg.com/pop-vender-static/qua/2018/spu/images/qua_mark_refimg_52.png\"></a></li><li class=\"ql-align-center\"><a href=\"https://item.jd.com/10023875891222.html#none\" target=\"_blank\" style=\"color: rgb(102, 102, 102);\">电信设备进网许可证</a></li><li><a href=\"https://item.jd.com/10023875891222.html\" target=\"_blank\" style=\"color: rgb(243, 2, 19);\"><img src=\"https://img30.360buyimg.com/sku/jfs/t1/176413/34/5316/271325/607d2bd8E258ccd03/6a85805a7f71ad3e.jpg\"></a></li><li><br></li></ul><p><br></p>', '2021-04-20 13:46:20', '2021-04-20 13:46:20');
INSERT INTO `item_desc` VALUES ('9', '<p>fsdfsadsaddsf</p>', '2021-05-21 17:28:27', '2021-05-21 17:28:27');
INSERT INTO `item_desc` VALUES ('10', '<p>123123</p>', '2021-05-21 17:30:04', '2021-05-21 17:30:04');
INSERT INTO `item_desc` VALUES ('11', '<p>3123</p>', '2021-05-21 17:38:55', '2021-05-21 17:38:55');
INSERT INTO `item_desc` VALUES ('12', '<p>3234</p>', '2021-05-21 17:42:04', '2021-05-21 17:42:04');

-- ----------------------------
-- Table structure for rights
-- ----------------------------
DROP TABLE IF EXISTS `rights`;
CREATE TABLE `rights` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(40) DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `path` varchar(50) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of rights
-- ----------------------------
INSERT INTO `rights` VALUES ('1', '管理员', '0', null, '1', '2021-02-18 19:17:23', '2021-02-18 19:17:23');
INSERT INTO `rights` VALUES ('2', '用户列表', '1', '/user', '2', '2021-02-18 19:22:19', '2021-02-18 19:22:19');
INSERT INTO `rights` VALUES ('3', '图书管理', '0', null, '1', '2021-02-18 19:22:41', '2021-02-18 19:22:41');
INSERT INTO `rights` VALUES ('4', '书籍管理', '3', '/item', '2', '2021-02-18 19:23:12', '2021-02-18 19:23:12');
INSERT INTO `rights` VALUES ('6', '书籍目录', '3', '/itemCat', '2', '2021-02-18 19:25:00', '2021-02-18 19:25:00');
INSERT INTO `rights` VALUES ('7', '借阅管理', '0', '', '1', '2021-02-18 19:23:44', '2021-02-18 19:23:44');
INSERT INTO `rights` VALUES ('8', '其他', '0', null, '1', '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES ('9', '借阅列表', '7', '/borrow', '2', '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES ('10', '定阅确认', '7', '/subscription', '2', '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES ('11', '用户列表-新增按钮', '2', null, '3', '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES ('12', '用户列表-修改按钮', '2', null, '3', '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES ('13', '用户列表-删除按钮', '2', null, '3', '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES ('15', '异常日志', '8', '/report', '2', '2021-02-18 19:24:12', '2021-02-18 19:24:12');
INSERT INTO `rights` VALUES ('16', '二', '8', '/reportCat', '2', '2021-08-11 20:10:31', '2021-08-11 20:10:35');

-- ----------------------------
-- Table structure for subscription
-- ----------------------------
DROP TABLE IF EXISTS `subscription`;
CREATE TABLE `subscription` (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `authorname` varchar(20) DEFAULT NULL,
  `bookname` varchar(50) DEFAULT NULL,
  `username` varchar(20) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  `endtime` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of subscription
-- ----------------------------
INSERT INTO `subscription` VALUES ('1', '老子', '道德经', '杨杰', null, '2021-08-12 15:42:01', '2021-08-28 15:42:05');
INSERT INTO `subscription` VALUES ('3', '老子', '道德经', '杨杰', null, '2021-08-12 15:42:01', '2021-08-28 15:42:05');
INSERT INTO `subscription` VALUES ('4', '老子', '道德经', '杨杰', null, '2021-08-12 15:42:01', '2021-08-28 15:42:05');

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(80) DEFAULT NULL,
  `password` varchar(80) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `created` datetime DEFAULT NULL,
  `updated` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('14', 'admin', '202cb962ac59075b964b07152d234b70', '12345678901', null, '1', '2021-08-14 11:03:44', null);
INSERT INTO `user` VALUES ('15', 'yangjie', 'e10adc3949ba59abbe56e057f20f883e', '12345678901', null, '1', '2021-08-15 22:20:37', null);
INSERT INTO `user` VALUES ('16', 'wwwwww', '202cb962ac59075b964b07152d234b70', '12345678901', null, '1', '2021-08-15 22:20:37', null);
INSERT INTO `user` VALUES ('17', 'tonylaoshi', '202cb962ac59075b964b07152d234b70', '12345678912', '1125@qq.com', '1', '2021-08-24 19:26:56', null);
