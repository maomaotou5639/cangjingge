package com.lib.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lib.pojo.Item;

public interface ItemMapper extends BaseMapper<Item> {
}
