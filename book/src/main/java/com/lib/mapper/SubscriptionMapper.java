package com.lib.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lib.pojo.Subscription;

public interface SubscriptionMapper extends BaseMapper<Subscription> {

}
