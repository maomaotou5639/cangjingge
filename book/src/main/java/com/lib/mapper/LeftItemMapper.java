package com.lib.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lib.pojo.LeftItem;

public interface LeftItemMapper extends BaseMapper<LeftItem> {
}
