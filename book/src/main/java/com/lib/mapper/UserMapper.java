package com.lib.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lib.pojo.User;

import java.util.List;

public interface UserMapper extends BaseMapper<User> {
    List<User> getUserList(String aa, int startindex, int pageSize);



}
