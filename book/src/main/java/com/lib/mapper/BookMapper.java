package com.lib.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lib.pojo.BookInfo;

public interface BookMapper extends BaseMapper<BookInfo> {
}
