package com.lib.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lib.pojo.ItemCat;

public interface ItemCatMapper extends BaseMapper<ItemCat> {
}
