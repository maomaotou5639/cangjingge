package com.lib.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lib.pojo.Rights;

public interface RightsMapper extends BaseMapper<Rights> {
}
