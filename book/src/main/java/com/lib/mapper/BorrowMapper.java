package com.lib.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lib.pojo.Borrow;

public interface BorrowMapper extends BaseMapper<Borrow> {
}
