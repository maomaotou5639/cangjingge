package com.lib.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.lib.pojo.Consumer;

public interface ConsumerMapper extends BaseMapper<Consumer> {
}
