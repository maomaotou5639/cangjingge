package com.lib.controller;

import com.lib.pojo.Subscription;
import com.lib.service.subscription.SubscriptionService;
import com.lib.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/subscription")
@CrossOrigin
public class SubscriptionController {
    @Autowired
    private SubscriptionService subscriptionService;

    @RequestMapping("/list")
    public SysResult SubscriptionList(){
        List list = subscriptionService.subscriptionList();
        return SysResult.success(list);
    }

    @DeleteMapping("/deleteId/{id}")
    public SysResult deleteId(@PathVariable Integer id){
        subscriptionService.deleteId(id);
        return SysResult.success();
    }

    //根据id去查,然后把订阅的信息存到借阅表
    @GetMapping("/getusername/{id}")
    public SysResult postId(Subscription subscription){
        subscriptionService.SubscriptionName(subscription);
        return SysResult.success();
    }


}
