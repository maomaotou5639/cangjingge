package com.lib.controller;

import com.lib.pojo.ItemCat;
import com.lib.service.ItemCatService;
import com.lib.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/itemCat")
public class ItemCatController {
    @Autowired
    private ItemCatService itemCatService;

    /**
     * 请求路径: /itemCat/findItemCatList
     * itemCat/findItemCatList/3
     * 请求类型: get
     * 请求参数: level
     * 业务说明: 查询3级分类菜单数据 要求三层结构嵌套
     * 返回值: SysResult对象
     */
    @GetMapping("/findItemCatList/{level}")
    public SysResult findItemCatList(@PathVariable Integer level) {
         List<ItemCat> itemCatList= itemCatService.findItemCatList(level);

        return SysResult.success(itemCatList);
    }


    /**
     * 请求路径: /itemCat/status/{id}/{status}
     * 请求类型: put
     * 请求参数:id status 可以用对象接
     * 返回值: SysResult对象
     *
     */
    @PutMapping("/status/{id}/{status}")
    //put请求不需要@RequestBody
    public SysResult updateStatus( ItemCat itemCat) {
        itemCatService.updateStatus(itemCat);
        return SysResult.success();
    }

    /**
     * 请求路径: /itemCat/saveItemCat
     * 请求类型: post
     * 请求参数: 表单数据
     * 返回值: SysResult对象
     */
    @PostMapping("/saveItemCat")
    public SysResult addItemCatForm(@RequestBody ItemCat itemCat) {
        itemCatService.addItemCatForm(itemCat);
        return SysResult.success();
    }

    /**
     * 实现商品分类删除
     * 请求路径: /itemCat/deleteItemCat
     * 请求类型: delete
     * 业务描述: 当删除节点为父级时,应该删除自身和所有的子节点
     * 请求参数:itemCat
     * 返回值: SysResult对象
     */
    @DeleteMapping("/deleteItemCat")
    public SysResult deleteItemCat(ItemCat itemCat) {
        itemCatService.deleteItemCat(itemCat);
        return SysResult.success();
    }

    /**
     * 请求路径: /itemCat/updateItemCat
     * 请求类型: put
     * 请求参数: 表单数据 ItemCat对象
     * 返回值: SysResult对象
     */

    @PutMapping("/updateItemCat")
    public SysResult updateItemCat(@RequestBody ItemCat itemCat) {
        itemCatService.updateItemCat(itemCat);
        return SysResult.success();
    }





}
