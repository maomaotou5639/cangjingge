package com.lib.controller;

import com.lib.pojo.BookInfo;
import com.lib.service.book.BookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/b")
public class BookController {
    @Autowired
    private BookService bookService;

    @RequestMapping("/book")
    public List<BookInfo> bookList(){
        return bookService.bookList();
    }


}
