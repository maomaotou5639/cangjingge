package com.lib.controller;

import com.lib.pojo.Consumer;
import com.lib.service.consumer.ConsumerService;
import com.lib.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/consumer")
//前端注册
//这儿需要改。
public class ConsumerController {

    @Autowired
    private ConsumerService consumerService;

    //注册
    @RequestMapping("/addConsumer")
    public SysResult addConsumer(@RequestBody Consumer consumer){
        consumerService.addConsumer(consumer);
        return SysResult.success();
    }

    //登录
    @PostMapping("login")
    public SysResult aslogin(@RequestBody Consumer consumer){
        String token = consumerService.aslogin(consumer);
        if(token == null || token.length() == 0){
            return SysResult.fail();
        }
        return SysResult.success(token);
    }



}
