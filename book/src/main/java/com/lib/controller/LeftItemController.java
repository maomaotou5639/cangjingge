package com.lib.controller;

import com.lib.mapper.LeftItemMapper;
import com.lib.pojo.LeftItem;
import com.lib.pojo.Rights;
import com.lib.service.LeftItemService;
import com.lib.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
@RequestMapping("/left")
public class LeftItemController {
    @Autowired
    private LeftItemService leftItemService;
    @GetMapping("getLeftItemList")
    public SysResult getRightsList(Integer level){

        List<LeftItem> leftItemList= leftItemService.getLeftList(level);
        return SysResult.success(leftItemList);
    }


}
