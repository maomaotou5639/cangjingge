package com.lib.controller;
import com.lib.pojo.Borrow;
import com.lib.service.BorrowService;
import com.lib.vo.PageResult;
import com.lib.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin
@RequestMapping("/borrow")
public class BorrowController {
    @Autowired
    private BorrowService borrowService;
    @GetMapping("/list")
    public SysResult getBorrowList(PageResult pageResult){
        pageResult=borrowService.getBorrowList(pageResult);
//        System.out.println(pageResult.getPageNum());
//        System.out.println(pageResult.getPageSize());
        return SysResult.success(pageResult);
    }
    @PostMapping("/addBorrow")
    public SysResult addBorrow(@RequestBody Borrow borrow){
        borrowService.addBorrow(borrow);
        return SysResult.success(borrow);
    }
    @PutMapping("/status/{id}/{status}")
    public SysResult updateStatus(Borrow borrow){
        borrowService.updateStatus(borrow);
        return SysResult.success();
    }
    @GetMapping("/{id}")
    public SysResult getBorrowById(@PathVariable Integer id){
        Borrow borrow=borrowService.getBorrowById(id);
        return SysResult.success(borrow);
    }
    @PutMapping("updateBorrow")
    public SysResult updateBorrow(@RequestBody Borrow borrow){
        borrowService.updateBorrow(borrow);
        return SysResult.success();
    }
    @DeleteMapping("/{id}")
    public SysResult deleteBorrow(@PathVariable Integer id){
        borrowService.deleteBorrow(id);
        return SysResult.success();
    }
}