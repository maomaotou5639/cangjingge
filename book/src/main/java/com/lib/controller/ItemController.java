package com.lib.controller;

import com.lib.service.ItemService;
import com.lib.vo.PageResult;
import com.lib.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@RequestMapping("/item")
public class ItemController {
    @Autowired
    private ItemService itemService;


    /**
     * 请求路径: /itemCatParam/findItemCatParamListByType?itemCatId=564&paramType=1
     * 请求类型: get
     * 业务描述: 根据商品分类和参数类型,查询信息
     * 请求参数:SysResult(pageResult)
     */
    @GetMapping("/getItemList")
    public SysResult getItemList(PageResult pageResult) {
        pageResult = itemService.getItemList(pageResult);
        return SysResult.success(pageResult);
    }
}
