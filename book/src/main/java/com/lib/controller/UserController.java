package com.lib.controller;

import com.lib.pojo.User;
import com.lib.service.UserService;
import com.lib.vo.PageResult;
import com.lib.vo.SysResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController //返回值都是JSON串
@CrossOrigin //跨域
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/aslogin")
    public SysResult login(@RequestBody User user){
        //System.out.println(user);
        String token = userService.login(user);
        //System.out.println("token "+token);
        //如果token为空 说明 ，后台返回数据失败
        if (token == null || token.length() == 0){ //位置都不能换
            return SysResult.fail();
        }
        //System.out.println("执行完成！");
        return SysResult.success(token);

    }
/**需求：利用分页展现user列表数据
 * user/list  get
 * 返回值： sysreslut对象(pageResult对象)*/
    @GetMapping("/list")
    public SysResult getUserList(PageResult pageResult){ //以pageResult来封装
        pageResult = userService.getUserLIst(pageResult);
        //System.out.println(SysResult.success(pageResult));
        return SysResult.success(pageResult);
    }
/**需求：list?query=&pageNum=1&pageSize=5
 * 创建一个RightsService类来接收前窜传过来的东西，但是还有两个值没有被接收
 * 但是每个给前端的响应还有格式要求，所以通过RightsService set另外两个值
 * 加上封装的三个值，一共五个值封装到SYSResult中的data里，然后返给前端*/

    //修改状态
    @PutMapping("/status/{id}/{status}")
    public SysResult uqdateUserStatus(User user){

        userService.uqdateUserStatus(user);
        return SysResult.success();
    }

    //增加
    @PostMapping("/addUser")
    public SysResult insertUserList(@RequestBody User user){
        userService.insertUserList(user);
        return SysResult.success(user);
    }
    //删除
    //@Transactional      //牛皮，加上注解后 运行异常的话就不能删除了，不加注解就算报错也删除
    @DeleteMapping("/{id}")
    public SysResult deleteUserId(User user){

//        try {
//            userService.deleteUserId(user);
//            //做个处理，处理异常返回给用户 但只是说删除不成功，不给他看我们的错误、
//            //运行错误来自service层 加过@transactional 保证事务回滚，
//
//        }catch (Exception e){
//            e.printStackTrace();
//            return SysResult.fail();
//        }
//        //但是：如果这样的异常处理多了就麻烦，这儿引入了全局异常处理方式
        userService.deleteUserId(user);
        return SysResult.success();
    }
    //id查为了修改查询
    //@Transactional //aop控制的方法出现 运行时异常 的话程序会回滚，不会执行
                    //如果不是运行时异常 是编译异常等待 就不会回滚
    @GetMapping("/{id}")
    public SysResult selectUserId(@PathVariable Integer id){
        User user = userService.selectUserId(id);

        return SysResult.success(user);
    }

    //修改
    @PutMapping("/updateUser")
    public SysResult updateUserId(@RequestBody User user){

        userService.updateUserId(user);

        return SysResult.success(user);
    }


}
