package com.lib.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


//配置类,替代xml配置文件,功能是一样的
@Configuration
public class MybatisPlusConfig {
    /**xml标签管理对象，将对象交给spring容器来管理，
     * 现在 将方法的返回值交给spring容器管理， @Bean注解
     *
     * MP分页：需要设定一个拦截器，将分页的sql进行动态的拼接
     * 有了这个拦截器，才能拼接sql后面给定的条件，不加就不会拼接*/
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor(){
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new PaginationInnerInterceptor(DbType.MARIADB));
        return interceptor;
    }


}
