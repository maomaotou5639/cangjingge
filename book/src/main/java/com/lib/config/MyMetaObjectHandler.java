package com.lib.config;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

import java.util.Date;


// MyBatise 对外暴露了一个自动填充的接口MetaObjectHandler接口
// 实现了MP的自动填充时间
@Component //将对象交给spring容器管理
public class MyMetaObjectHandler implements MetaObjectHandler {
    Date date = new Date();
    @Override
    public void insertFill(MetaObject metaObject) {
        //自动填充字段 自动填充值 固定写法
        this.setFieldValByName("created",date,metaObject);
        this.setFieldValByName("uptated",date,metaObject);
    }

    @Override
    public void updateFill(MetaObject metaObject) {
        this.setFieldValByName("uptated",new Date(),metaObject);
    }
}
