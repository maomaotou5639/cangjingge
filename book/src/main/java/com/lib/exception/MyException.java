package com.lib.exception;

import com.lib.vo.SysResult;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

//全局异常处理类,内部采用环绕通知的方式
@RestControllerAdvice /*这个的作用： 异常处理后采用JSON串的方式返回
捕获的是controller层的异常，就算是别的层出现异常了，也能捕获，因为controller层
会调用别的层*/
public class MyException {
    //要返回信息给用户，那返回啥呢？规定要返回统一的SYSResult信息 status=201 msg=失败


    //注解说明：在遇到某种异常时 全局异常处理机制有效
    @ExceptionHandler(value = RuntimeException.class) //其中value在这儿可以省略
    public Object exception(Exception e){
        e.printStackTrace(); //打印异常信息
        return SysResult.fail(); //返回特定的响应信息

    }

}
