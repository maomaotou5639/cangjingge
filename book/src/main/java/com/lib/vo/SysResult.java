package com.lib.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

@Data//get 和 set
@Accessors(chain = true)
@NoArgsConstructor
@AllArgsConstructor
public class SysResult {
   //主要是实现前后端交互的

   private  Integer status; //状态码信息
   private String msg; //提示信息
   private Object data; //封装业务数据

    public static SysResult fail(){
        return new SysResult(201,"后台返回数据失败",null);
    }

    public static SysResult success(){
        return new SysResult(200,"后台返回数据成功",null);
    }

    public static SysResult success(Object data){
        return new SysResult(200,"后台返回数据成功",data);
    }

    public static SysResult success(String msg,Object data){
        return new SysResult(200,msg,data);
    }


}
