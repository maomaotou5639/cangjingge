package com.lib.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("subscription")
public class Subscription {
    @TableId(type = IdType.AUTO)
    private Integer id;
    private String authorname;
    private String bookname;
    private String username;
    private String phone;
    private Date time;
    private Date endtime;

}
