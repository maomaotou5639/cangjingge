package com.lib.pojo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.List;

@TableName("book_cat") //windows系统 字母大小写 不敏感  Linux系统 严格区分大小写
@Data
@Accessors(chain = true)
public class LeftItem {
    private Integer id;
    private String name;
    private Integer parentId;
    private Integer level;
    @TableField(exist = false)
    private List<LeftItem> children; //不是表格固有属性
}
