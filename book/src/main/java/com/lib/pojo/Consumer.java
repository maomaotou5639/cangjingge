package com.lib.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("consumer")
public class Consumer {

    @TableId(type = IdType.AUTO)
    private Integer id;
    private String email;
    private String password;

    private String phone;
    private String username;
    private Boolean Status;




}
