package com.lib.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("book_info")
public class BookInfo { //这里没继承时间处理接口
    @TableId(type = IdType.AUTO)
    private Integer bookId;
    private String name;
    private String author;
    private String introduction;
    private String language;
    private String price;
    private Integer parentId;
    private Integer number;
    private String isbn;
}
