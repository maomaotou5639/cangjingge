package com.lib;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("com.lib.mapper")//为包下的接口克隆对象，为了其他地方能直接调用它的方法

public class SpringbootRun {
    public static void main(String[] args) {
        SpringApplication.run(SpringbootRun.class,args);
    }
}

