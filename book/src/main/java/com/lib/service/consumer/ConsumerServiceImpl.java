package com.lib.service.consumer;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lib.mapper.ConsumerMapper;
import com.lib.pojo.Consumer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.util.UUID;

@Service
public class ConsumerServiceImpl implements ConsumerService{

    @Autowired
    private ConsumerMapper consumerMapper;

    @Override
    public void addConsumer(Consumer consumer) {
        consumer.setStatus(true);
        //UUID加密
        String password = consumer.getPassword();
        String passwordsStr = DigestUtils.md5DigestAsHex(password.getBytes());
        consumer.setPassword(passwordsStr);

        consumerMapper.insert(consumer);
    }
    //
    @Override
    public String aslogin(Consumer management) {
        //加密:1、获取原始密码
        String password = management.getPassword();
        //2、将密码进行加密的处理
        String passwordsStr = DigestUtils.md5DigestAsHex(password.getBytes());
        //3、将密文传递给对象
        management.setPassword(passwordsStr);
        //4、根据条件查询
        QueryWrapper<Consumer> queryWrapper = new QueryWrapper(management);

        Consumer ManagementDB = consumerMapper.selectOne(queryWrapper);//查到了就有值，没查到就为空
        //System.out.println("userDB"+userDB);
        //5、定义token数据
        String uuid = UUID.randomUUID().toString().replace("-","");


        return ManagementDB==null?null:uuid;

    }

    @Override
    public Consumer consumerId(Integer id) {
        return consumerMapper.selectById(id);
    }


}
