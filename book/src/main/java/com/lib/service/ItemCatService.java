package com.lib.service;

import com.lib.pojo.ItemCat;

import java.util.List;

public interface ItemCatService {
    List<ItemCat> findItemCatList(Integer level);

    void updateStatus(ItemCat itemCat);

    void addItemCatForm(ItemCat itemCat);

    void deleteItemCat(ItemCat itemCat);

    void updateItemCat(ItemCat itemCat);
}
