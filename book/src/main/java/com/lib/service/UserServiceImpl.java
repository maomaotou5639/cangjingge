package com.lib.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lib.mapper.UserMapper;
import com.lib.pojo.User;
import com.lib.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.DigestUtils;

import java.util.List;
import java.util.UUID;

@Service
public class UserServiceImpl implements UserService {
    @Autowired

    private UserMapper usermapper;

    @Override
    public String login(User user) {
        //加密:1、获取原始密码
        String password = user.getPassword();
        //2、将密码进行加密的处理
        String passwordsStr = DigestUtils.md5DigestAsHex(password.getBytes());
        //3、将密文传递给对象
        user.setPassword(passwordsStr);
        //4、根据条件查询
        QueryWrapper<User> queryWrapper = new QueryWrapper(user);

        User userDB = usermapper.selectOne(queryWrapper);//查到了就有值，没查到就为空
        //System.out.println("userDB"+userDB);
        //5、定义token数据
        String uuid = UUID.randomUUID().toString().replace("-","");


        return userDB==null?null:uuid;
    }

    @Override
    public PageResult getUserLIst(PageResult pageResult) {

        long total = usermapper.selectCount(null);//语句条数
        pageResult.setTotal(total);

        int pageNum = pageResult.getPageNum();
        int pageSize = pageResult.getPageSize();
        int startindex = (pageNum-1) * pageSize;   //起始位置的算法
        String aa = pageResult.getQuery();
        List<User> rows = usermapper.getUserList(aa,startindex,pageSize);
        pageResult.setRows(rows);
        return pageResult;
    }

    @Override
    public void uqdateUserStatus(User user) {
        usermapper.updateById(user); //把对象中的id当做查询条件，然后另一个属性当做set条件
    }

    @Override
    @Transactional //事务回滚注解
    //插入数据
    public void insertUserList(User user) {

        //加密:1、获取原始密码
        String password = user.getPassword();
        //2、将密码进行加密的处理
        String passwordsStr = DigestUtils.md5DigestAsHex(password.getBytes());
        //3、将密文传递给对象
        user.setPassword(passwordsStr);

//加密存入 以及修改初始状态
        user.setStatus(true);
        usermapper.insert(user);
//

    }
//删除
    @Override
    @Transactional
    public void deleteUserId(User user) {
        //int a = 1/0;
        usermapper.deleteById(user);
    }

    //修改
    @Override
    public void updateUserId(User user) {

        usermapper.updateById(user);

    }
//为了删除查id
    @Override
    public User selectUserId(Integer id) {
        return usermapper.selectById(id);
    }


}
