package com.lib.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lib.mapper.ItemCatMapper;
import com.lib.pojo.ItemCat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class ItemCatImpl implements com.lib.service.ItemCatService {
    @Autowired
    private ItemCatMapper itemCatMapper;

    //优化方法
    /*
     *1.准备Map集合,实现数据封装
     * Map<Key,Value> =Map<parentId,List<ItemCat>>
     *
     */
    public Map<Integer,List<ItemCat>> itemCatMap() {

        //1.定义map集合,一副扑克,一筐套娃
        Map<Integer, List<ItemCat>> map = new HashMap<>();
        //2.查询所有的数据库数据 1-2-3,初次查询没有层级结构
        List<ItemCat> list = itemCatMapper.selectList(null);
        for (ItemCat itemCat:
                list) {
            int parentId = itemCat.getParentId();
            if (map.containsKey(parentId)) {
                //有key 获取list集合中的内容 追加到该key的集合中
                List<ItemCat> exeList = map.get(parentId);
                exeList.add(itemCat);
            } else {
                //如果没有key,该数据第一次存入map,将自己封装为list中第一条记录
                List<ItemCat> firstList = new ArrayList<>();
                firstList.add(itemCat);
                map.put(parentId, firstList);
            }
        }
        return map;
    }






    //3.将map封装进List<ItemCat>,并且调整好父子级关系,还要将循环层数和level参数关联
    @Override
    public List<ItemCat> findItemCatList(Integer level) {
        long startTime = System.currentTimeMillis();
        //把上面准备好的map集合传进方法,把查询的一次性结果拿进服务端
        //一筐套娃放在办公室
        Map<Integer, List<ItemCat>> map = itemCatMap();
    //    1.开始遍历整理出父子级关系
        if (level == 1) {
            return map.get(0);
        }
        //2.获取二级菜单
        if (level == 2) {
            return getTwoList(map);
        }

        //3.获取三级菜单
        //3.1获取二级商品分类信息 BUG:有的数据可能没有子集,如何处理
        //下面语句取出的结果包含一级和二级
        List<ItemCat> oneList = getTwoList(map);
        for (ItemCat oneItemCat:oneList
             ) {
        //    从一级中获取二级菜单列表,然后才可以根据二级的id查三级的prid
            List<ItemCat> towList = oneItemCat.getChildren();
        //    bug解决,如果二级菜单没有子集,可以跳过本次循环
            if (towList == null || towList.size() == 0) {
                continue;
            }
            for (ItemCat towItemCat : towList
            ) {
                //查询三级菜单 条件:parentId=2级ID
                List<ItemCat> threeList = map.get(towItemCat.getId());
                towItemCat.setChildren(threeList);
            }

        }
        long endTime = System.currentTimeMillis();
        System.out.println("耗时:"+(endTime-startTime)+"毫秒");
        return oneList;
    }

    @Override
    @Transactional
    public void updateStatus(ItemCat itemCat) {
        itemCatMapper.updateById(itemCat);
    }

    @Override
    @Transactional
    public void addItemCatForm(ItemCat itemCat) {
        //为状态添加默认值
        itemCat.setStatus(true);
        itemCatMapper.insert(itemCat);
    }

    /**
     * 如果是父级,应该连带删除子级
     *
     * */
    @Override
    @Transactional
    public void deleteItemCat(ItemCat itemCat) {
        //1.判断是否为三级菜单
        if (itemCat.getLevel()==3) {
            //如果是三级.直接删除
            itemCatMapper.deleteById(itemCat.getId());
            return;//程序终止
        }
        //2.检查菜单是否为2级
        if (itemCat.getLevel() == 2) {
            //删除二级之前要先删除三级,根据pa_id=2级的id
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("parent_id", itemCat.getId());
            itemCatMapper.delete(queryWrapper);
            //再删除二级菜单
            itemCatMapper.deleteById(itemCat.getId());
        }

        //3.删除一级数据,先找出所有的二级菜单
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("parent_id",itemCat.getId());
        List<ItemCat> twoList = itemCatMapper.selectList(queryWrapper);
        for (ItemCat twoItemCat:twoList
             ) {
            queryWrapper.clear();
            queryWrapper.eq("parent_id", twoItemCat.getId());
            itemCatMapper.delete(queryWrapper);//删除3级
            itemCatMapper.deleteById(twoItemCat.getId());//删除二级
        }
        itemCatMapper.deleteById(itemCat.getId());

    }

    @Override
    public void updateItemCat(ItemCat itemCat) {
        itemCatMapper.updateById(itemCat);
    }



    public List<ItemCat> getTwoList(Map<Integer , List<ItemCat>> map) {
        List<ItemCat> oneList = map.get(0);
        for (ItemCat oneItemCat:oneList
             ) {
            List<ItemCat> twoList = map.get(oneItemCat.getId());
            oneItemCat.setChildren(twoList);
        }
        return oneList;
    }

    //1.循环嵌套查询方式,需要优化
    //@Override
    //public List<ItemCat> findItemCatList(Integer level) {
    //    long startTime = System.currentTimeMillis();
    //    QueryWrapper queryWrapper = new QueryWrapper();
    //    //1.查询以及目录
    //    //queryWrapper.clear();
    //    queryWrapper.eq("parent_id", 0);
    //    List<ItemCat> onelist = itemCatMapper.selectList(queryWrapper);
    //    //2.查询二级目录
    //    for (ItemCat oneItemCat : onelist) {
    //        queryWrapper.clear();
    //        queryWrapper.eq("parent_id", oneItemCat.getId());
    //        List<ItemCat> twolist = itemCatMapper.selectList(queryWrapper);
    //        //3.查询三级目录
    //        for (ItemCat twoItemCat:twolist) {
    //            queryWrapper.clear();
    //            queryWrapper.eq("parent_id", twoItemCat.getId());
    //            List<ItemCat> threelist = itemCatMapper.selectList(queryWrapper);
    //            //3.1三级查询结果封装进二级结果
    //            twoItemCat.setChildren(threelist);
    //        }
    //        //2.1二级查询结果封装进一级结果
    //        oneItemCat.setChildren(twolist);
    //
    //    }
    //
    //
    //
    //    long endTime = System.currentTimeMillis();
    //    System.out.println("用时"+(endTime-startTime)+"毫秒");
    //    return onelist;
    //}


}
