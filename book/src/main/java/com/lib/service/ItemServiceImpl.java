package com.lib.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lib.mapper.ItemMapper;
import com.lib.pojo.Item;
import com.lib.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class ItemServiceImpl implements ItemService {
    @Autowired
    private ItemMapper itemMapper;

    @Override
    public PageResult getItemList(PageResult pageResult) {
        //自己写的,实现了功能,再跟一遍老师的私立
        //List<Item> rows = itemMapper.selectList(null);
        //long total = rows.size();
        //pageResult.setRows(rows).setTotal(total);


        //老师,带查询
        //这一步直接用mp提供的API传入了total参数
        IPage page = new Page(pageResult.getPageNum(), pageResult.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        //模糊查询状态判断,看搜索框是否有query数据
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag, "title", pageResult.getQuery());
        page = itemMapper.selectPage(page,queryWrapper);
        long total = page.getTotal();
        List<Item> rows = page.getRecords();
        return pageResult.setTotal(total).setRows(rows);
    }
}
