package com.lib.service.book;

import com.lib.mapper.BookMapper;
import com.lib.pojo.BookInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BookServiceImpl implements BookService{
    @Autowired
    private BookMapper bookMapper;

    @Override
    public List<BookInfo> bookList() {

        return bookMapper.selectList(null);
    }
}
