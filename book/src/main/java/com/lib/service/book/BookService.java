package com.lib.service.book;

import com.lib.pojo.BookInfo;

import java.util.List;

public interface BookService {
    List<BookInfo> bookList();

}
