package com.lib.service.subscription;


import com.lib.mapper.BorrowMapper;
import com.lib.mapper.SubscriptionMapper;
import com.lib.pojo.Borrow;
import com.lib.pojo.Subscription;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SubscriptionServiceImpl implements SubscriptionService{

    @Autowired
    private SubscriptionMapper subscriptionMapper;

    @Autowired
    private BorrowMapper borrowMapper;

    @Override
    public List subscriptionList() {
        return subscriptionMapper.selectList(null);
    }

    @Override
    public void deleteId(Integer id) {
        subscriptionMapper.deleteById(id);
    }

    //在订阅里查到数据,在借阅里插入
    @Override
    public void SubscriptionName(Subscription subscription) {
        Subscription subscription1 = subscriptionMapper.selectById(subscription.getId());

        //往借阅表里插数据
        Borrow borrow = new Borrow();
        borrow.setPhone(subscription1.getPhone()).setUsername(subscription1.getUsername())
                .setTime(subscription1.getTime()).setBookname(subscription1.getBookname());

        try {
            borrowMapper.insert(borrow);
            Thread.sleep(1);
        } catch (InterruptedException e) {
            e.printStackTrace();
            //按照时间延迟处理 ，还未处理
        }
        subscriptionMapper.deleteById(subscription1.getId());
    }





}
