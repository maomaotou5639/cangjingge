package com.lib.service;

import com.lib.pojo.Rights;

import java.util.List;

public interface RightsService {
    List<Rights> getRightsList();
}
