package com.lib.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lib.mapper.LeftItemMapper;
import com.lib.pojo.LeftItem;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class LeftItemServiceImpl implements LeftItemService{
    @Autowired
    private LeftItemMapper leftItemMapper;
    @Override
    public List<LeftItem> getLeftList(Integer level) {
        QueryWrapper<LeftItem> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id",0);
        List<LeftItem> oneList = leftItemMapper.selectList(queryWrapper);
        for (LeftItem oneLeftItem : oneList){
            queryWrapper.clear();
            queryWrapper.eq("parent_id",oneLeftItem.getId());
            List<LeftItem> children = leftItemMapper.selectList(queryWrapper);
            oneLeftItem.setChildren(children);
        }
        return oneList;
    }
}
