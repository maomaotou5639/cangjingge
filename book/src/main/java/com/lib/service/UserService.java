package com.lib.service;

import com.lib.pojo.User;
import com.lib.vo.PageResult;


public interface UserService {


    String login(User user);

    PageResult getUserLIst(PageResult pageResult);

    void uqdateUserStatus(User user);

    void insertUserList(User user);

    void deleteUserId(User user);

    void updateUserId(User user);

    User selectUserId(Integer id);



    //PageResult getUserList(PageResult pageResult);
}
