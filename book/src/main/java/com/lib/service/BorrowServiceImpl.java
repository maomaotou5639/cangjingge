package com.lib.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.lib.mapper.BorrowMapper;
import com.lib.pojo.Borrow;
import com.lib.vo.PageResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.List;

@Service
public class BorrowServiceImpl implements BorrowService{
    @Autowired
    private BorrowMapper borrowMapper;

    @Override
    public PageResult getBorrowList(PageResult pageResult) {
        IPage iPage = new Page(pageResult.getPageNum(), pageResult.getPageSize());
        QueryWrapper queryWrapper = new QueryWrapper();
        boolean flag = StringUtils.hasLength(pageResult.getQuery());
        queryWrapper.like(flag, "username", pageResult.getQuery());
        iPage = borrowMapper.selectPage(iPage,queryWrapper);
        long total = iPage.getTotal();
//        long total = borrowMapper.selectCount(null);
        List<Borrow> rows = iPage.getRecords();
        return pageResult.setTotal(total).setRows(rows);
    }
    @Override
    public void addBorrow(Borrow borrow) {
        borrowMapper.insert(borrow);
    }

    @Override
    public void updateStatus(Borrow borrow) {
        borrowMapper.updateById(borrow);
    }

    @Override
    public Borrow getBorrowById(Integer id) {
        return borrowMapper.selectById(id);
    }

    @Override
    public void updateBorrow(Borrow borrow) {
        borrowMapper.updateById(borrow);
    }

    @Override
    public void deleteBorrow(Integer id) {
        borrowMapper.deleteById(id);
    }
}