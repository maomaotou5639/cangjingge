package com.lib.service;

import com.lib.pojo.Borrow;
import com.lib.vo.PageResult;

public interface BorrowService {
    PageResult getBorrowList(PageResult pageResult);

    void addBorrow(Borrow borrow);

    void updateStatus(Borrow borrow);

    Borrow getBorrowById(Integer id);

    void updateBorrow(Borrow borrow);

    void deleteBorrow(Integer id);
}