package com.lib.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.lib.mapper.RightsMapper;
import com.lib.pojo.Rights;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RightsServiceImpl implements com.lib.service.RightsService {

    @Autowired
    private RightsMapper rightsMapper;

    /**
     * 1.查询所有的1级信息 parent_id=0
     * 2.查询1级下边的2级信息 parent_id=1级id
     * @return
     */
    @Override
    public List<Rights> getRightsList() {
        //1.查询一级权限信息
        QueryWrapper<Rights> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("parent_id",0);
        //返回1级的时候 只有一级菜单   那么2级菜单在哪了???
        List<Rights> oneList = rightsMapper.selectList(queryWrapper);
        //2.查询1级下边的二级list集合
        for (Rights oneRights : oneList){
            queryWrapper.clear(); //将where条件清空之后重新添加
            queryWrapper.eq("parent_id",oneRights.getId());
            List<Rights> children = rightsMapper.selectList(queryWrapper);
            //查询子级之后,将数据赋值给父级 实现嵌套结构
            oneRights.setChildren(children);
        }
        return oneList;
    }
}
