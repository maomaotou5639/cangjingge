package com.lib.service;

import com.lib.pojo.LeftItem;
import com.lib.pojo.Rights;

import java.util.List;

public interface LeftItemService {
    List<LeftItem> getLeftList(Integer level);
}
