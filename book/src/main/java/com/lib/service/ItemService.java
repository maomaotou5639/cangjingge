package com.lib.service;

import com.lib.vo.PageResult;

public interface ItemService  {
    PageResult getItemList(PageResult pageResult);
}
